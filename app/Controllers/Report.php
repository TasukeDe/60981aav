<?php

namespace App\Controllers;

use App\Models\ReportModel;
use Aws\S3\S3Client;

class Report extends BaseController


{
    public function index() //Обображение всех записей
    {
        if (!$this->ionAuth->loggedIn()) {
            return redirect()->to('/auth/login');
        }
        $model = new ReportModel();
        $data['report'] = $model->getReport();
        echo view('report/all_reports', $this->withIon($data));
    }

    public function view($id = null) //отображение одной записи
    {
        if (!$this->ionAuth->loggedIn()) {
            return redirect()->to('/auth/login');
        }
        $model = new ReportModel();
        $data['report'] = $model->getReport($id);
        echo view('report/view', $this->withIon($data));
    }

    public function viewUserReports()
    {
        if (!$this->ionAuth->loggedIn())
        {
            return redirect()->to('/auth/login');
        }

        $model = new ReportModel();
//        $data['report'] = $model->getReport($id);
        $data['report'] = $model->getUserReports($this->ionAuth->user()->row()->id);
        echo view('report/user_all_reports', $this->withIon($data));
    }

    public function viewAllWithUsers()
    {
        if ($this->ionAuth->isAdmin()) {
            //Подготовка значения количества элементов выводимых на одной странице
            if (!is_null($this->request->getPost('per_page'))) //если кол-во на странице есть в запросе
            {
                //сохранение кол-ва страниц в переменной сессии
                session()->setFlashdata('per_page', $this->request->getPost('per_page'));
                $per_page = $this->request->getPost('per_page');
            } else {
                $per_page = session()->getFlashdata('per_page');
                session()->setFlashdata('per_page', $per_page); //пересохранение в сессии
                if (is_null($per_page)) $per_page = '5'; //кол-во на странице по умолчанию
            }
            $data['per_page'] = $per_page;
            //Обработка запроса на поиск
            if (!is_null($this->request->getPost('search'))) {
                session()->setFlashdata('search', $this->request->getPost('search'));
                $search = $this->request->getPost('search');
            } else {
                $search = session()->getFlashdata('search');
                session()->setFlashdata('search', $search);
                if (is_null($search)) $search = '';
            }
            $data['search'] = $search;
            helper(['form', 'url']);
            $model = new ReportModel();
            $data['report'] = $model->getReportWithUser(null, $search)->paginate($per_page, 'group1');
            $data['pager'] = $model->pager;
            echo view('report/view_all_with_users', $this->withIon($data));
        } else {
            session()->setFlashdata('message', lang('Curating.admin_permission_needed'));
            return redirect()->to('/auth/login');
        }
    }

    public function create()
    {
        if (!$this->ionAuth->loggedIn()) {
            return redirect()->to('/auth/login');
        }
        helper(['form']);
        $data ['validation'] = \Config\Services::validation();
        $data['user_id'] = $this->ionAuth->user()->row()->user_id;
        echo view('report/create', $this->withIon($data));
    }

    public function store()
    {
        helper(['form', 'url']);

        if ($this->request->getMethod() === 'post' && $this->validate([
                'number_car' => 'required|min_length[6]|max_length[6]',
                'region_car' => 'required|min_length[2]|max_length[3]',
                'country' => 'required|min_length[3]|max_length[3]',
                'description' => 'required|max_length[70]',
                'picture_url' => 'is_image[picture_url]',
                'picture_url2' => 'is_image[picture_url2]',
                'picture_url3' => 'is_image[picture_url3]',
            ])) {
            $insert = null;
            $insert2 = null;
            $insert3 = null;

            $file = $this->request->getFile('picture_url');
            $file2 = $this->request->getFile('picture_url2');
            $file3 = $this->request->getFile('picture_url3');

            if ($file->getSize() != 0) {
                //подключение хранилища
                $s3 = new S3Client([
                    'version' => 'latest',
                    'region' => 'us-east-1',
                    'endpoint' => getenv('S3_ENDPOINT'),
                    'use_path_style_endpoint' => true,
                    'credentials' => [
                        'key' => getenv('S3_KEY'), //чтение настроек окружения из файла .env
                        'secret' => getenv('S3_SECRET'), //чтение настроек окружения из файла .env
                    ],
                ]);
                //получение расширения имени загруженного файла
                $ext = explode('.', $file->getName());
                $ext = $ext[count($ext) - 1];

                $ext2 = explode('.', $file2->getName());
                $ext2 = $ext2[count($ext2) - 1];

                $ext3 = explode('.', $file3->getName());
                $ext3 = $ext3[count($ext3) - 1];
                //загрузка файла в хранилище
                $insert = $s3->putObject([
                    'Bucket' => getenv('S3_BUCKET'), //чтение настроек окружения из файла .env
                    //генерация случайного имени файла
                    'Key' => getenv('S3_KEY') . '/file' . rand(0, 999999) . $this->millitime() . '1.' . $ext,
                    'Body' => fopen($file->getRealPath(), 'r+')
                ]);

                $insert2 = $s3->putObject([
                    'Bucket' => getenv('S3_BUCKET'), //чтение настроек окружения из файла .env
                    //генерация случайного имени файла
                    'Key' => getenv('S3_KEY') . '/file' . rand(0, 999999) . $this->millitime() .'2.' . $ext2,
                    'Body' => fopen($file2->getRealPath(), 'r+')
                ]);

                $insert3 = $s3->putObject([
                    'Bucket' => getenv('S3_BUCKET'), //чтение настроек окружения из файла .env
                    //генерация случайного имени файла
                    'Key' => getenv('S3_KEY') . '/file' . rand(0, 999999) . $this->millitime() . '3.' . $ext3,
                    'Body' => fopen($file3->getRealPath(), 'r+')
                ]);
            }

            $data = [
                'user_id' => $this->request->getPost('user_id'),
                'number_car' => strtoupper($this->request->getPost('number_car')),
                'region_car' => $this->request->getPost('region_car'),
                'country' => strtoupper($this->request->getPost('country')),
                'description' => $this->request->getPost('description'),
            ];

            if (!is_null($insert)) {
                $data['picture_url'] = $insert['ObjectURL'];
            }
            if (!is_null($insert2)) {
                $data['picture_url2'] = $insert2['ObjectURL'];
            }
            if (!is_null($insert3)) {
                $data['picture_url3'] = $insert3['ObjectURL'];
            }

            $model = new ReportModel();
            $model->save($data);

            session()->setFlashdata('message', lang('Curating.report_create_success'));
            return redirect()->to('/report');
        } else {
            return redirect()->to('/report/create')->withInput();
        }
    }

    public function edit($id)
    {
        if (!$this->ionAuth->loggedIn()) {
            return redirect()->to('/auth/login');
        }
        $model = new ReportModel();

        helper(['form']);
        $data ['report'] = $model->getReport($id);
        $data ['validation'] = \Config\Services::validation();
        echo view('report/edit', $this->withIon($data));

    }

    public function update()
    {
        helper(['form', 'url']);
        echo '/report/edit/' . $this->request->getPost('id');
        if ($this->request->getMethod() === 'post' && $this->validate([
                'id' => 'required',
                'number_car' => 'required|min_length[6]|max_length[6]',
                'region_car' => 'required|min_length[2]|max_length[3]',
                'country' => 'required|min_length[3]|max_length[3]',
                'description' => 'required',
            ])) {
            $model = new ReportModel();
            $model->save([
                'id' => $this->request->getPost('id'),
                'number_car' => $this->request->getPost('number_car'),
                'region_car' => $this->request->getPost('region_car'),
                'country' => $this->request->getPost('country'),
                'description' => $this->request->getPost('description'),
            ]);

            return redirect()->to('/report');
        } else {
            return redirect()->to('/report/edit/' . $this->request->getPost('id'))->withInput();
        }
    }

    public function delete($id)
    {
        if (!$this->ionAuth->loggedIn()) {
            return redirect()->to('/auth/login');
        }
        $model = new ReportModel();
        $model->delete($id);
        return redirect()->to('/report');
    }

    public function millitime()
    {
        $microtime = microtime();
        $comps = explode(' ', $microtime);
        // Note: Using a string here to prevent loss of precision
        // // in case of "overflow" (PHP converts it to a double)
        return sprintf('%d%03d', $comps[1], $comps[0] * 1000);
    }
}
