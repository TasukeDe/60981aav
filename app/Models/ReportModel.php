<?php namespace App\Models;
use CodeIgniter\Model;
class ReportModel extends Model
{
    protected $table = 'reports'; //таблица, связанная с моделью
    protected $allowedFields = ['region_car', 'description', 'report_status', 'number_car', 'country', 'user_id', 'picture_url',
                                'picture_url2', 'picture_url3'];
    public function getReport($id = null)
    {
        if (!isset($id)) {
            return $this->findAll();
        }
        return $this->where(['id' => $id])->first();
    }

    public function getReportWithUser($id = null, $search = '')
    {   
        $builder = $this->select('reports.*, users.username, users.email')
        ->join('users','reports.user_id = users.id')
        ->like('username', $search,'both', null, true)->orlike('description',$search,'both',null,true);
        if (isset($id))
        {
            return $builder->where(['reports.user_id' => $id])->first();
        }
        return $builder;
    }

    public function getUserReports($userId)
    {
        return $this->where(['user_id' => $userId])->findAll();
    }
}