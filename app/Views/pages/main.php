<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
    <div class="jumbotron text-center">
        <img class="mb-4" src="https://avatars.githubusercontent.com/u/35006749?s=400&u=2ac72fe68291ddb5ea7b15da113ee1639201535e&v=4" alt="" width="156" height="156"><h1 class="display-4">BadDriver</h1>
        <p class="lead">Это приложение поможет вам улучшить ваш город.</p>
        <a class="btn btn-danger btn-lg" href="auth/login" role="button">Войти</a>
    </div>
<?= $this->endSection() ?>
