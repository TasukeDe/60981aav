<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
<div class="container main">
<?php if (!empty($report) && is_array($report)) : ?>
    <h2>Все жалобы:</h2>
    <div class="d-flex justify-content-between mb-2">
        <?= $pager->links('group1','my_page') ?>
        <?= form_open('report/viewAllWithUsers', ['style' => 'display: flex']); ?>
        <select name="per_page" class="ml-3" aria-label="per_page">
            <option value="2" <?php if($per_page == '2') echo("selected"); ?>>2</option>
            <option value="5"  <?php if($per_page == '5') echo("selected"); ?>>5</option>
            <option value="10" <?php if($per_page == '10') echo("selected"); ?>>10</option>
            <option value="20" <?php if($per_page == '20') echo("selected"); ?>>20</option>
        </select>
        <button class="btn btn-outline-success" type="submit" class="btn btn-danger">На странице</button>
        </form>
        <?= form_open('report/viewAllWithUsers',['style' => 'display: flex']); ?>
        <input type="text" class="form-control ml-3" name="search" placeholder="Имя или описание" aria-label="Search"
               value="<?= $search; ?>">
        <button class="btn btn-outline-success" type="submit">Найти</button>
        </form>
    </div>
    <table class="table table-striped">
        <thead>
            <th scope="col">Картинка</th>
            <th scope="col">Email создателя</th>
            <th scope="col">Имя</th>
            <th scope="col">Номер машины</th>
            <th scope="col">Код региона</th>
            <th scope="col">Код страны</th>
            <th scope="col">Описание</th>
            <th scope="col">Управление</th>
        </thead>
        <tbody>
    <?php foreach ($report as $item): ?>
        <tr>
            <td>
                <img height="150" src="<?= esc($item['picture_url']); ?>" class="card-img" alt="<?= esc($item['number_car']); ?>">
            </td>
            <td>
                <?= esc($item['email']); ?>
            </td>
            <td>
                <?= esc($item['username']); ?>
            </td>
            <td>
                <?= esc($item['number_car']); ?>
            </td>
            <td>
                <?= esc($item['region_car']); ?>
            </td>
            <td>
                <?= esc($item['country']); ?>
            </td>
            <td>
                <?= esc($item['description']); ?>
            </td>
            <td>
                <a href="<?= base_url()?>/report/view/<?= esc($item['id']); ?>" class="btn btn-primary btn-sm">Просмотреть</a>
                <a href="<?= base_url()?>/report/edit/<?= esc($item['id']); ?>" class="btn btn-warning btn-sm">Редактировать</a>
                <a href="<?= base_url()?>/report/delete/<?= esc($item['id']); ?>" class="btn btn-danger btn-sm">Удалить</a>
            </td>
        </tr>
    <?php endforeach; ?>
        </tbody>
        </table>

<?php else : ?>
    <?= form_open('report/viewAllWithUsers',['style' => 'display: flex']); ?>
        <input type="text" class="form-control ml-3" name="search" placeholder="Имя или описание" aria-label="Search"
               value="<?= $search; ?>">
        <button class="btn btn-outline-success" type="submit" class="btn btn-primary">Найти</button>
    </form>
    <div class="text-center">
        <p>Жалобы не найдены </p>
        <a class="btn btn-primary btn-lg" href="<?= base_url()?>/report/create">Создать жалобу</a>
        <img height="450" src="/46472-lurking-cat.gif" class="card-img">
    </div>
<?php endif ?>
</div>
<?= $this->endSection() ?>