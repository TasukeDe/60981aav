<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
<div class="container main">
    <?php use CodeIgniter\I18n\Time; ?>
    <?php if (!empty($report)) : ?>
        <div class="d-flex justify-content-center">
            <div class="card mb-3" style="max-width: 540px;">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card-body">
                            <div class="d-flex justify-content-between">
                                <div class="card-title">Номер машины:</div>
                                <div class="text-muted"><?= esc($report['number_car']); ?></div>
                            </div>
                            <div class="d-flex justify-content-between">
                                <div class="card-title">Регион:</div>
                                <div class="text-muted"><?= esc($report['region_car']); ?></div>
                            </div>
                            <div class="d-flex justify-content-between">
                                <div class="card-title">Код страны:</div>
                                <div class="text-muted"><?= esc($report['country']); ?></div>
                            </div>
                            <div class="d-flex justify-content-between">
                                <div class="card-title">Описание жалобы:</div>
                                <p class="text-muted"><?= esc($report['description']); ?></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row d-flex justify-content-center">
            <h2>Доказательства нарушения</h2>
        </div>
        <div class="row d-flex justify-content-center">
            <div class="col-md-3">
                <img height="200" width="200" src="<?= esc($report['picture_url']); ?>" class="card-img" alt="Фотография не загружена">
            </div>
            <div class="col-md-3">
                <img height="200" width="200" src="<?= esc($report['picture_url2']); ?>" class="card-img" alt="Фотография не загружена">
            </div>
            <div class="col-md-3">
                <img height="200" width="200" src="<?= esc($report['picture_url3']); ?>" class="card-img" alt="Фотография не загружена">
            </div>
        </div>
    </div>

    <?php else : ?>
        <p>Жалоба не найдена.</p>
        <img height="450" src="/46472-lurking-cat.gif" class="card-img">
    <?php endif ?>
</div>
<?= $this->endSection() ?>