<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>

<div class="container main  ">
    <h2 class="d-flex justify-content-center">Все мои жалобы</h2>

    <?php if (!empty($report) && is_array($report)) : ?>

        <?php foreach ($report as $item): ?>
            <div class="d-flex justify-content-center">
                <div class="card mb-3" style="max-width: 540px; min-width: 540px;">
                    <div class="row">
                        <div class="col-md-4 d-flex align-items-center">
                            <?php if (is_null($item['picture_url'])) :?>
                                <img height="150" src="https://www.flaticon.com/svg/static/icons/svg/2555/2555013.svg" class="card-img" alt="<?= esc($item['number_car']); ?>">
                            <?php else : ?>
                                <img height="150" src="<?= esc($item['picture_url']); ?>" class="card-img" alt="<?= esc($item['number_car']); ?>">
                            <?php endif ?>
                        </div>

                        <div class="col-md-8">
                            <div class="card-body">
                                <h2 class="card-title"><?= esc($item['number_car']); ?></h2>
                                <span class="card-text"><?= esc($item['region_car']); ?></span>
                                <span class="card-text"><?= esc($item['country']); ?></span>
                                <img src="https://www.flaticon.com/svg/static/icons/svg/3909/3909301.svg"
                                     alt="RUS" height="50">
                                <p class="card-text"><?= esc($item['description']); ?></p>
                                <a href="<?= base_url()?>/index.php/report/view/<?= esc($item['id']); ?>" class="btn btn-danger">Просмотреть</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    <?php else : ?>
        <p class="d-flex justify-content-center">Невозможно найти жалобы.</p>
        <img height="450" src="/46472-lurking-cat.gif" class="card-img">
    <?php endif ?>
</div>
<?= $this->endSection() ?>
