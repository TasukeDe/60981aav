<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>

<div class="container" style="max-width: 540px;">

    <?= form_open_multipart('report/update'); ?>
    <input type="hidden" name="id" value="<?= $report["id"] ?>">

    <div class="form-group">
        <label for="name">Номер машины</label>
        <input type="text" class="form-control <?= ($validation->hasError('number_car')) ? 'is-invalid' : ''; ?>" name="number_car"
               value="<?= $report["number_car"]; ?>">
        <div class="invalid-feedback">
            <?= $validation->getError('number_car') ?>
        </div>

    </div>
    <div class="form-group">
        <label for="name">Код региона</label>
        <input type="text" class="form-control <?= ($validation->hasError('region_car')) ? 'is-invalid' : ''; ?>" name="region_car"
               value="<?= $report["region_car"]; ?>">
        <div class="invalid-feedback">
            <?= $validation->getError('region_car') ?>
        </div>

    </div>
    <div class="form-group">
        <label for="name">Код страны</label>
        <input type="text" class="form-control <?= ($validation->hasError('country')) ? 'is-invalid' : ''; ?>" name="country"
               value="<?= $report["country"]; ?>">
        <div class="invalid-feedback">
            <?= $validation->getError('country') ?>
        </div>

    </div>
    <div class="form-group">
        <label for="name">Описание</label>
        <input type="text" class="form-control <?= ($validation->hasError('description')) ? 'is-invalid' : ''; ?>" name="description"
               value="<?= $report["description"] ?>">
        <div class="invalid-feedback">
            <?= $validation->getError('description') ?>
        </div>

    </div>
    <div class="form-group">
    <button type="submit" class="btn btn-primary" name="submit">Сохранить</button>
    </div>
    </form>
    </div>
<?= $this->endSection() ?>