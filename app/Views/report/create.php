<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
<div class="container" style="max-width: 540px;">

    <?= form_open_multipart('report/store'); ?>
    <div class="form-group">
        <input type="hidden" class="form-control" name="user_id"
               value="<?= $user_id ?>">
        <div class="invalid-feedback">
            <?= $validation->getError('number_car') ?>
        </div>
    </div>
    <div class="form-group">
        <label for="name">Номер машины (формат: A123BC, лат.)</label>
        <input placeholder="A123BC" pattern="[A-Za-z]{1}[0-9]{3}[A-Za-z]{2}"
               style="text-transform: uppercase;"
               type="text" class="form-control <?= ($validation->hasError('number_car')) ? 'is-invalid' : ''; ?>" name="number_car"
               value="<?= old('number_car'); ?>">
        <div class="invalid-feedback">
            <?= $validation->getError('number_car') ?>
        </div>
    </div>
    <div class="form-group">
        <label for="name">Код региона (формат: 86/777)</label>
        <input placeholder="777" pattern="[0-9]{2,3}"
               type="text" class="form-control <?= ($validation->hasError('region_car')) ? 'is-invalid' : ''; ?>" name="region_car"
               value="<?= old('region_car'); ?>">
        <div class="invalid-feedback">
            <?= $validation->getError('region_car') ?>
        </div>
    </div>
    <div class="form-group">
        <label for="name">Код страны</label>
        <input placeholder="RUS" type="text" style="text-transform: uppercase;"
               class="form-control <?= ($validation->hasError('country')) ? 'is-invalid' : ''; ?>" name="country"
               value="<?= old('country'); ?>">
        <div class="invalid-feedback">
            <?= $validation->getError('country') ?>
        </div>
    </div>
    <div class="form-group">
        <label for="name">Описание</label>
        <input type="text" class="form-control <?= ($validation->hasError('description')) ? 'is-invalid' : ''; ?>" name="description"
               value="<?= old('description'); ?>">
        <div class="invalid-feedback">
            <?= $validation->getError('description') ?>
        </div>
    </div>
    <div class="form-group">
        <label for="picture_url">Изображения</label>
        <input type="file" class="form-control-file mb-3 <?= ($validation->hasError('picture_url')) ? 'is-invalid' : ''; ?>" name="picture_url">
        <div class="invalid-feedback">
            <?= $validation->getError('picture_url') ?>
        </div>

        <input type="file" class="form-control-file mb-3 <?= ($validation->hasError('picture_url2')) ? 'is-invalid' : ''; ?>" name="picture_url2">
        <div class="invalid-feedback">
            <?= $validation->getError('picture_url2') ?>
        </div>

        <input type="file" class="form-control-file <?= ($validation->hasError('picture_url3')) ? 'is-invalid' : ''; ?>" name="picture_url3">
        <div class="invalid-feedback">
            <?= $validation->getError('picture_url3') ?>
        </div>
    </div>
    
    <div class="form-group">
    <button type="submit" class="btn btn-primary" name="submit">Создать</button>
    </div>
    </form>


    </div>
<?= $this->endSection() ?>