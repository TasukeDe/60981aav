<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class AddReportPictures extends Migration
{
	public function up()
	{
        if ($this->db->tableexists('reports'))
        {
            $this->forge->addColumn('reports',array(
                'picture_url2' => array('type' => 'VARCHAR', 'constraint' => '255', 'null' => TRUE),
                'picture_url3' => array('type' => 'VARCHAR', 'constraint' => '255', 'null' => TRUE)
            ));
        }
	}

	public function down()
	{
        $this->forge->dropColumn('reports', 'picture_url2');
        $this->forge->dropColumn('reports', 'picture_url3');
    }
}
