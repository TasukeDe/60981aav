<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Report extends Migration
{
	public function up()
	{
		//
		if (!$this->db->tableexists('reports'))
        {
            // Setup Keys
            $this->forge->addkey('id', TRUE);

            $this->forge->addfield(array(
                'id' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE, 'auto_increment' => TRUE),
                'user_id' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE),
                'number_car' => array('type' => 'VARCHAR', 'constraint' => '20', 'null' => FALSE),
                'region_car' => array('type' => 'VARCHAR', 'constraint' => '20', 'null' => FALSE),
                'country' => array('type' => 'VARCHAR', 'constraint' => '20', 'null' => FALSE),
                'description' => array('type' => 'VARCHAR', 'constraint' => '100', 'null' => FALSE)
            ));
            $this->forge->addForeignKey('user_id','users','id','RESTRICT','RESRICT');
            // create table
            $this->forge->createtable('reports', TRUE);
        }

		if (!$this->db->tableexists('achievements'))
        {
            // Setup Keys
            $this->forge->addkey('id', TRUE);

            $this->forge->addfield(array(
                'id' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE, 'auto_increment' => TRUE),
                'description' => array('type' => 'VARCHAR', 'constraint' => '100', 'null' => FALSE)
            ));
            // create table
            $this->forge->createtable('achievements', TRUE);
        }
	}

	public function down()
	{
		//
		$this->forge->droptable('reports');
        $this->forge->droptable('achievements');
	}
}
