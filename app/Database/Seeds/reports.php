<?php namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class Reports extends Seeder
{
   public function run()
   {
        $data = [
                'user_id' => 1,
                'number_car' => 'A123AA',
                'region_car' => '86',
                'country' => 'RUS',
                'description'=>'First report',
        ];
        $this->db->table('reports')->insert($data);

        $data = [
            'user_id' => 1,
            'number_car' => 'B456BB',
            'region_car' => '777',
            'country' => 'RUS',
            'description'=>'Second report',
        ];
        $this->db->table('reports')->insert($data);
      }
}